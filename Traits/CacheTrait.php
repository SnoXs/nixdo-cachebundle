<?php

namespace Nixdo\CacheBundle\Traits;

use Nixdo\CacheBundle\Exception\MissingServiceInjectionException;
use Nixdo\CacheBundle\Interfaces\CacheInterface;

trait CacheTrait {

    protected $cache = null;

    public function cacheAdd($key, $value) {
        if ($this->getCache() !== null) {
            return $this->getCache()->add($key, $value);
        } else {
            throw new MissingServiceInjectionException("Cache", get_class());
        }
    }

    public function cacheSet($key, $value) {
        if ($this->getCache() !== null) {
            return $this->getCache()->set($key, $value);
        } else {
            throw new MissingServiceInjectionException("Cache", get_class());
        }
    }

    public function cacheGet($key) {
        if ($this->getCache() !== null) {
            return $this->getCache()->get($key);
        } else {
            throw new MissingServiceInjectionException("Cache", get_class());
        }
    }

    public function cacheDelete($key) {
        if ($this->getCache() !== null) {
            return $$this->getCache()->delete($key);
        } else {
            throw new MissingServiceInjectionException("Cache", get_class());
        }
    }

    public function cacheGetAll() {
        if ($this->getCache() !== null) {
            return $this->getCache()->getAll();
        } else {
            throw new MissingServiceInjectionException("Cache", get_class());
        }
    }

    public function cacheIsLoaded() {
        if ($this->getCache() === null) {
            return false;
        } else {
            return $this->getCache()->isLoaded();
        }
    }

    public function setCache(CacheInterface $cache) {
        $this->cache = $cache;
        return $this;
    }

    public function getCache() {
        if ($this->cache === null) {
            if (property_exists($this, 'container')) {
                if (!$this->container->has('cache')) {
                    throw new \LogicException('The Cache service is not registered in your application.');
                }
                $this->cache = $this->container->get('cache');
            } else if (method_exists($this, "getContainer")) {
                $this->cache = $this->getContainer()->get('cache');
            }
        }
        return $this->cache;
    }

}

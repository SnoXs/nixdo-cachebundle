<?php

namespace Nixdo\CacheBundle\Services;

use Nixdo\CacheBundle\Interfaces\CacheInterface;
use Nixdo\CacheBundle\Interfaces\CacheSystemInterface;
use Nixdo\CacheBundle\Exception\CacheRuntimeException;

class Cache implements CacheInterface {

    protected $cacheSystem;

    public function add($key, $value) {
        if ($this->cacheSystem === null) {
            throw new CacheRuntimeException("You must set a CacheSystem to add a key.");
        }
        return $this->cacheSystem->add($key, $value);
    }

    public function set($key, $value) {
        if ($this->cacheSystem === null) {
            throw new CacheRuntimeException("You must set a CacheSystem to add a key.");
        }
        return $this->cacheSystem->set($key, $value);
    }

    public function get($key) {
        if ($this->cacheSystem === null) {
            throw new CacheRuntimeException("You must set a CacheSystem to get a key.");
        }
        return $this->cacheSystem->get($key);
    }

    public function getAll() {
        if ($this->cacheSystem === null) {
            throw new CacheRuntimeException("You must set a CacheSystem to get a key.");
        }
        return $this->cacheSystem->getAll();
    }

    public function delete($key) {
        if ($this->cacheSystem === null) {
            throw new CacheRuntimeException("You must set a CacheSystem to delete a key.");
        }
        return $this->cacheSystem->delete($key);
    }

    public function isLoaded() {
        if ($this->cacheSystem === null) {
            return false;
        } else {
            return $this->cacheSystem->isLoaded();
        }
    }

    public function setCacheSystem(CacheSystemInterface $cacheSystem) {
        $this->cacheSystem = $cacheSystem;
        return $this;
    }

    public function getCacheSystem() {
        return $this->cacheSystem;
    }

    public function __construct() {
        $this->cacheSystem = null;
    }

}

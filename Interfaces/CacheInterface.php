<?php

namespace Nixdo\CacheBundle\Interfaces;

interface CacheInterface {

    public function add($key, $value);

    public function set($key, $value);

    public function get($key);

    public function getAll();

    public function delete($key);

    public function isLoaded();
}
